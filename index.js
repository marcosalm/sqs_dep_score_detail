const database = require('database');

const QInsertDetail = '\
  INSERT INTO sp_score_wallet_detail (id_dynamo, eventtypeid, originid, `fields`, `values`) \
    VALUES (?, ?, ?, ?, ?)';

const QUpdateDetail = '\
  UPDATE sp_score_wallet_detail \
    SET id_dynamo = ?, eventtypeid = ?, originid = ?, `fields` = ?, `values` = ? \
    WHERE id = ?';

/**
 * Inserts Detail on database
 */
const insertDetail = (conn, { dynamoId, eventTypeId, originId, fields, values }) => {
  return new Promise((resolve, reject) => {
    conn.query(QInsertDetail, [dynamoId, eventTypeId, originId, fields, values], (err, row) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(row.insertId);
    });
  });
};

/**
 * Update Detail on database
 */
const updateDetail = (conn, { detailId, dynamoId, eventTypeId, originId, fields, values }) => {
  return new Promise((resolve, reject) => {
    conn.query(QUpdateDetail, [dynamoId, eventTypeId, originId, fields, values, detailId], (err) => {
      if (err) { return reject(database.isIntegrityError(err).error); }
      return resolve(detailId);
    });
  });
};

/**
 * Formats detail data to be used on functions
 */
const formatDetail = (dynamoId, eventTypeId, originId, fields, values, detailId) => {
  const separator = ';';
  const reducer = (acc, value) => {
    if (acc) { return `${acc}${separator}${value}`; }
    return value;
  };

  return {
    detailId,
    dynamoId,
    eventTypeId,
    originId,
    fields: fields && fields.length ? fields.reduce(reducer) : '',
    values: values && values.length ? values.reduce(reducer) : ''
  };
};

/**
 * Saves Event Detail on Database
 */
const saveDetail = (detail) => {
  return new Promise((resolve, reject) => {
    let conn = null;
    database.getConnection()
      .then((connection) => {
        conn = connection;
        return !detail.detailId ? insertDetail(conn, detail) : updateDetail(conn, detail);
      })
      .then((detailId) => resolve(detailId))
      .catch((err) => reject(err))
      .then(() => {
        if (conn) {
          conn.release();
        }
      });
  });
};

module.exports.saveDetail = saveDetail;
module.exports.formatDetail = formatDetail;
